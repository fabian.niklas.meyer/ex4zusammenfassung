\section{Grundlegendes}
\subsection{Schwerpunktsbewegungen}
Die reduzierte Masse ist $\mu = \frac{m_1m_2}{m_1 + m_2}$.

Falls eines der beiden Teilchen deutlich leichter ist lässt sich dieser Ausdruck zu dem Vereinfachen.
Im Falle einer Relativbewegung zwischen Eletkron und Proton ist z.B.:
\[\mu = \frac{m_em_p}{m_e + m_p} \approx m_e\]

\subsection{Bragg-Bedingung}
\[n\lambda = 2d \, \sin(\vartheta)\]

\subsection{Dipole in Magnetfeldern}
Die Energie eines Dipols mit magnetischem Moment $\vec \mu$ in einem Magnetfeld der Stärke $\vec B$
ist gegeben durch:
\[E = -\vec \mu \cdot \vec B\]

Die Kraft welche auf einzelne Dipole wirkt ist aus der Energie gegeben mit:
\[\vec F = \grad (\vec \mu \cdot \vec B)\]

\subsection{Gyromagnetisches Verhältnis}
Das gyromagnetische Verhältnis $\vec\mu = \gamma \vec l$ beschreibt den Zusammenhang zwischen
magnetischem Moment $\vec \mu$ und dem Drehimpuls $\vec l$.

\subsection{Nützliche Zusammenhänge}
\begin{center}
    \begin{tabular}{|l|l|l|}
      \hline
      \textbf{Bezeichnung}        & \textbf{Formelzeichen}  & \textbf{Beziehungen}             \\ \hline
      Kreisfrequenz               & $\omega$                & $2\pi f$                         \\ \hline
      Wellenzahl                  & $k$                     & $\nicefrac{2\pi}{\lambda}$       \\ \hline
      Phasengeschwindigkeit       & $v_\mathrm{ph}$         & $\nicefrac \omega k$             \\ \hline
      Lichtgeschwindigkeit        & $c$                     & $c = f\lambda$                   \\ \hline
      Photonenimpuls              & p                       & $\frac h\lambda$                 \\ \hline
      $h$-reduziert               & $\hbar$                 & $\frac h{2\pi}$                  \\ \hline
      Spin eines Photons          & $s$                     & $\hbar$                          \\ \hline
      De-Broglie-Wellenlänge      & $\lambda$               & $\nicefrac hp$                   \\ \hline
      De-Broglie-Frequenz         & $\nu$                   & $\frac{E_\mathrm{Kin}}{h}$       \\ \hline
      Impuls Welle                & $p$                     & $p = \hbar k$                    \\ \hline
      Energie-Impuls-Bez.         & $E$                     & $m_0^2 c^4 + p^2 c^2$            \\ \hline
    \end{tabular}
\end{center}

\subsection{Quantenmechanische Addition von Drehimpulsen}
Seien $\vec j$ und $\vec l$ zwei Drehimpulse.
Dann ergibt sich sich für $z$ mit $\vec z = \vec j + \vec l$:

\[|j - l| \leq z \leq |j+l|\]

\subsection{Heissenbergsche Unschärferelation}
Es gilt:
\begin{align*}
  \Delta x \cdot \Delta p &\geq \frac \hbar 2 \\
  \Delta E \cdot \Delta t &\geq \frac \hbar 2
\end{align*}

Die Allgemeine Version ist gegen durch:
\[\operatorname{Var} (X)\cdot \operatorname{Var}(Y) = \frac 12 \cdot |\braket{\Psi|[\hat A,\hat B]|\Psi}|\]
mit dem Kommutator $[\hat A, \hat B] \coloneqq \hat A\hat B - \hat B \hat A$.


\subsection{Schrödinger-Gleichung}
Die (zeitunabhängige) Schrödinger-Gleichung als Eigenwertproblem ist gegeben durch:
\[ H\Psi = E\Psi \]
mit dem Hamiltonoperator
\[H = -\frac{\hbar^2}{2m} \laplace + V\]
und Potential $V$. Der Laplace-Operator ist definiert als die Spur einer Hesse-Matrix,
d.h. $\laplace\Psi \coloneqq \frac{\partial^2 \Psi}{\partial x^2} + \frac{\partial^2 \Psi}{\partial y^2} + \ldots$.

\subsection{Erwartungswerte und Operatoren}
Es lässt sich in der Quantenmechanik jeder Observablen $A$ ein (hermitischer, d.h. selbstadjungierter) Operator
$\hat A$ zuordnen, sodass für den Erwartungswert $\braket{A}$ dieser Observablen gilt:

\[\braket{A} = \int \braket{\Psi|\hat A|Psi}  = \int \Psi^* \hat A \Psi \]

Wenn $\Psi$ eine Eigenfunktion/Eigenvektor zum Operator $\hat A$ und Eigenwert $A$ ist, d.h.
$A\Psi = \hat A \Psi$, dann ist die Oberservable $A$ scharf definiert.
D.h. alle nach der zeitungabhängigen Schrödingergleichung gegebenen Wellen haben eine scharf definierte Energie.
(Intuitiv unbeschränkte Lebenszeit, daher scharf definierte Energie).

Einige Operatoren:

\begin{center}
  \begin{tabular}{|l|l|l|}
    \hline
    \textbf{Zeichen} & \textbf{Definition} & \textbf{Bedeutung} \\ \hline
    $p_a$ & $-i\hbar \frac{\partial}{\partial a}$ & \text{Drehimpuls in Richtung $a$} \\ \hline
    $\vec p$ & $-i\hbar \grad$ & Drehimpuls \\ \hline
    $\hat {\vec L}$ & $-i\hbar (\vec r \times \grad ) $ & Drehimpuls \\ \hline
    $H$ & $-\frac{\hbar^2}{2m}\laplace + V$ & Hamiltonoperator ($\Ekin + \Epot$) \\ \hline
    $E$ & $-i\hbar \frac{\partial}{\partial t}$ & {Gesamtenergie} \\ \hline
  \end{tabular}
\end{center}

\subsection{Integration in Kugelkoordinaten}
Die Reihenfolge bei Integration in Kugelkoordinaten ist relevant, als auch
der Zusatsterm $r^2 \sin (\vartheta)$. Über ganz $\mathbb R^3$ integriert man mittels
\[\int_0^\infty \int_0^{2\pi} \int_0^{\pi} \! \Psi_{n,l,m} (r,\vartheta, \varphi) r^2 \sin (\varphi) \, \mathrm d\varphi\, \mathrm
d\vartheta \, \mathrm dr\]


\subsection{Der Drehimpuls}
Im folgenden werden häufig Drehimpulse durch eine Quantenzahl $l$ festgelegt.
Die Beträge des assozierten Drehimpulses ergeben sich damit dann jeweils zu:
\[\sqrt{j(j+1)} \hbar\]

\subsection{Gase}
Bei Gasen gilt das ideale Gasgesetz:
\[\frac NV = \frac{p}{k_B T}\]

Die mittlere Relativgeschwindigkeit zweier Gase $A,B$ ist:
\[\overline v_{AB} = \sqrt{\frac{8k_B T}{\pi\mu}} \text{ mit } \mu = \frac{m_A m_B}{m_A + m_B}\]
